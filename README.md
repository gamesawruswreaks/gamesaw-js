# GameSaw JS #
GameSaw JS is a HTML5/JavaScript game framework, it uses the HTML5 element <canvas> to render 2d and 3d graphics, as well as serving some basic event methods and a phlethora of gamedev help classes and methods such as collision detection, pathfinding, image manipulation, audio playback and more.
The Library will be used mostly for 2d games, but in the future 3d might come in more and more as HTML5 is getting better 3d capabilities with WebGL.

## Features ##
* [Core](Core) - Core game engine functionality
* [Events/Input](Events) - A General event system for mouse, keyboard and touch
* [Resource Management](Resource) - Helps keeping track on ingame resources, makes it easy to do loaders
* [Image/Sprites](Image) - For image manipulation, sprites, animations
* [Audio](Audio) - API for controlling samples and soundtrack in a natural way
* [Geometry](Geometry) - Basic geometry classes with collision detection
* [Particle Engine](Particle) - Blazing particle engine powered by WebGL
* [Basic Physics](Physics) - A simple physics engine that can be evolved
* [Helpers](Helpers) - Such as Pathfinding and Random generators, as well as basic trigonometric methods
* [Unit Testing](Testing) - Basic Assert and Test suites