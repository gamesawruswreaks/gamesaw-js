/* *
 * Entity
 * *
 * The gamesaw entity class is a placeholder class for a game entity.
 * these can be many things, and JavaScript is the perfect language to
 * handle entity based games. The entity is an Object that gets populated
 * with modules that can be used in games to build up components and parts
 * of a game.
 *
 */

GSAW.entity = {

	Entity : function(params) {
		var entity = {

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		entity.constructor(params);

		return entity;
	}
};