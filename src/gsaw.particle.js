/* *
 * Particle Engine
 * *
 * The gamesaw particle engine is a WebGL based particle engine for
 * the game library, this contains 4 specific classes that handles
 * different levels of the global particle engine.
 *
 */

GSAW.engine.particle = {

	Particle : function(params) {
		var particle = {

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		particle.constructor(params);

		return particle;
	},

	Emitter : function(params) {
		var emitter = {

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		emitter.constructor(params);

		return emitter;
	},

	System : function(params) {
		var system = {

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		system.constructor(params);

		return system;
	},

	Engine : function(params) {
		var engine = {

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		engine.constructor(params);

		return engine;
	}
};