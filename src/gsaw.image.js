/* *
 * Image
 * *
 * The GameSaw Image namespace is a collection of image related classes, this is of
 * course the image itself, but also animations, spritesheets, textures and the likes.
 * the namespace will also contain image manipulation methods such as multiply filter
 * and scale methods.
 *
 */

GSAW.image = {

	Image : function(params) {
		var image = {
			uid: 0,
			source: "",
			image: new Image(),
			width: 0,
			height: 0,
			loaded: false,
			manager: {},

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}

				if(this.source != undefined) {
					this.load(this.source);
				}
			},

			load : function(src) {
				this.source = src;
				this.image.setAttribute("src", src);
				if(this.manager != undefined) {
					this.uid = this.manager.addResource("image");
				}
				this.addEventListeners();
			},

			render : function(x, y) {
				$g.drawImage(this.image, x, y);
			},

			renderSize : function(x, y, width, height) {
				$g.drawImage(this.image, x, y, width, height);
			},

			load_handler : function(event) {
				this.loaded = true;
				if(this.manager != undefined) {
					this.manager.setLoaded(this.uid)
				}
			},

			error_handler : function(event) {
				$log.add("image", "Error loading image!");
			},

			addEventListeners : function() {
				var _this = this;
				this.image.addEventListener("load", function(e){_this.load_handler(e);});
				this.image.addEventListener("error", function(e){_this.error_handler(e);});
			}
		};
		image.constructor(params);

		return image;
	},

	Animation : function(params) {
		var animation = {
			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		animation.constructor(params);

		return animation;
	},

	SpriteSheet : function(params) {
		var spritesheet = {
			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		spritesheet.constructor(params);

		return spritesheet;
	},

	Sprite : function(params) {
		var sprite = {
			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		sprite.constructor(params);

		return sprite;
	}
};

GSImage = GSAW.image.Image;
GSAnimation = GSAW.image.Animation;
GSSpriteSheet = GSAW.image.SpriteSheet;
GSSprite = GSAW.image.Sprite;