/* *
 * Event
 * *
 * GameSaw Event keeps track on all the input that comes from the user,
 * this will be keyboard, mouse and touch events that will be useful to
 * track when doing games.
 *
 */

GSAW.event = {
	KEY : {
		"BACKSPACE" : 8, "TAB" : 9, "ENTER" : 13, "SHIFT" : 16, "CTRL" : 17,
		"ALT" : 18, "PAUSE" : 19, "CAPS" : 20, "ESCAPE" : 27, "PAGEUP" : 33,
		"PAGEDOWN" : 34, "END" : 35, "HOME" : 36,
		"LEFT" : 37, "UP" : 38, "RIGHT" : 39, "DOWN" : 40, "INSERT" : 45, "DELETE" : 46,
		"0" : 48, "1" : 49, "2" : 50, "3" : 51, "4" : 52, "5" : 53, "6" : 54,
		"7" : 55, "8" : 56, "9" : 57,
		"A" : 65, "B" : 66, "C" : 67, "D" : 68, "E" : 69, "F" : 70, "G" : 71,
		"H" : 72, "I" : 73, "J" : 74, "K" : 75, "L" : 76, "M" : 77, "N" : 78,
		"O" : 79, "P" : 80, "Q" : 81, "R" : 82, "S" : 83, "T" : 84, "U" : 85,
		"V" : 86, "W" : 87, "Y" : 88, "X" : 89, "Z" : 90,
		"LEFTSUPER" : 91, "RIGHTSUPER" : 92, "SELECT" : 93,
		"NUM0" : 96, "NUM1" : 97, "NUM2" : 98, "NUM3" : 99, "NUM4" : 100,
		"NUM5" : 101,"NUM6" : 102, "NUM7" : 103, "NUM8" : 104, "NUM9" : 105,
		"MULTIPLY" : 106, "ADD" : 107, "SUBSTRACT" : 108, "DECIMAL" : 110,
		"DIVIDE" : 111, "F1" : 112, "F2" : 113, "F3" : 114, "F4" : 115, "F5" : 116,
		"F7" : 118, "F8" : 119, "F9" : 120, "F10" : 121, "F11" : 122, "F12" : 123,
		"NUMLOCK" : 144, "SCROLLLOCK" : 145, "SEMICOLON" : 186, "EQUAL" : 187,
		"COMMA" : 188, "DASH" : 189, "PERIOD" : 190, "FORWARDSLASH" : 191,
		"GRAVEACCENT" : 192, "OPENBRACKET" : 219, "BACKSLASH" : 220, "CLOSEBRAKET" : 221,
		"SINGLEQUOTE" : 222
	},

	GLYPH : {
		8: "BACKSPACE", 9: "TAB", 13: "ENTER", 16: "SHIFT", 17: "CTRL", 18: "ALT", 19: "PAUSE", 
		20: "CAPS", 27: "ESCAPE", 33: "PAGEUP", 34: "PAGEDOWN", 35: "END", 36: "HOME", 37: "LEFT", 
		38: "UP", 39: "RIGHT", 40: "DOWN", 45: "INSERT", 46: "DELETE", 48: "0", 49: "1", 50: "2", 
		51: "3", 52: "4", 53: "5", 54: "6", 55: "7", 56: "8", 57: "9", 65: "A", 66: "B", 67: "C", 
		68: "D", 69: "E", 70: "F", 71: "G", 72: "H", 73: "I", 74: "J", 75: "K", 76: "L", 77: "M", 
		78: "N", 79: "O", 80: "P", 81: "Q", 82: "R", 83: "S", 84: "T", 85: "U", 86: "V", 87: "W", 
		88: "Y", 89: "X", 90: "Z", 91: "LEFTSUPER", 92: "RIGHTSUPER", 93: "SELECT", 96: "NUM0", 
		97: "NUM1", 98: "NUM2", 99: "NUM3", 100: "NUM4", 101: "NUM5", 102: "NUM6", 103: "NUM7", 
		104: "NUM8", 105: "NUM9", 106: "MULTIPLY", 107: "ADD", 108: "SUBSTRACT", 110: "DECIMAL", 
		111: "DIVIDE", 112: "F1", 113: "F2", 114: "F3", 115: "F4", 116: "F5", 118: "F7", 119: "F8", 
		120: "F9", 121: "F10", 122: "F11", 123: "F12", 144: "NUMLOCK", 145: "SCROLLLOCK", 
		186: "SEMICOLON", 187: "EQUAL", 188: "COMMA", 189: "DASH", 190: "PERIOD", 
		191: "FORWARDSLASH", 192: "GRAVEACCENT", 219: "OPENBRACKET", 220: "BACKSLASH", 
		221: "CLOSEBRAKET", 222: "SINGLEQUOTE"
	},

	MouseManager : function(params) {
		var mouseManager = {
			x : 0,
			y : 0,
			button : [3],
			click : [3],
			prevent_default : true,
			stop_propagation : true,
			disable_context : true,
			container : {},
			parent : {},
			log_type : "mouse",

			init : function(params) {
				var _this = this;
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}

				this.button[0] = false;
				this.button[1] = false;
				this.button[2] = false;

				this.click[0] = false;
				this.click[1] = false;
				this.click[3] = false;

				$log.add(this.log_type, "Initializing mouse events.");

				this.container.addEventListener("mousemove", function(event) {
					_this.setMousePosition(_this, event);
				}, true);

				this.container.addEventListener("mousedown", function(event) {
					_this.setMouseDown(_this, event);
				}, true);

				this.container.addEventListener("mouseup", function(event) {
					_this.setMouseUp(_this, event);
				}, true);

				this.container.addEventListener("click", function(event) {
					_this.setClick(_this, event);
				}, true);

				this.container.addEventListener("contextmenu", function(event){
					if(_this.disable_context) {
						event.preventDefault();
					}
				}, false);
			},

			resetClicks : function() {
				this.click[0] = false;
				this.click[1] = false;
				this.click[2] = false;
			},

			setMouseDown : function(_this, event) {
				if(_this.prevent_default) {
					event.preventDefault();
				}
				if(_this.stop_propagation) {
					event.stopPropagation();
				}
				_this.button[event.button] = true;
			},
			
			setMouseUp : function(_this, event) {
				if(_this.prevent_default) {
					event.preventDefault();
				}
				if(_this.stop_propagation) {
					event.stopPropagation();
				}
				_this.button[event.button] = false;
			},

			setClick : function(_this, event) {
				if(_this.prevent_default) {
					event.preventDefault();
				}
				if(_this.stop_propagation) {
					event.stopPropagation();
				}
				_this.click[event.button] = true;
			},
		
			setMousePosition : function(_this, event) {
				if (event.pageX != undefined && event.pageY != undefined) {
			    	_this.x = event.pageX;
			    	_this.y = event.pageY;
			    }
			    else {
			    	_this.x = e.clientX + document.body.scrollLeft +
			            document.documentElement.scrollLeft;
			    	_this.y = e.clientY + document.body.scrollTop +
			            document.documentElement.scrollTop;
			    }
			    
			    _this.x -= _this.container.offsetLeft;
			    _this.y -= _this.container.offsetTop;
			}
		};
		mouseManager.init(params);

		return mouseManager;
	},

	KeyManager : function(params) {
		var keyManager = {
			keys : [256],
			container : {},
			parent : {},
			prevent_default : false,
			stop_propagation : false,
			key_logger : true,

			init : function(params) {
				var _this = this;
				var i = 0;
				var len = 256;
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}

				for(i; i < len; i += 1) {
					this.keys[i] = false;
				}

				$log.add("keyboard", "Initializing key manager.");

				window.addEventListener("keydown", function(event) {
					_this.setKeyDown(_this, event);
				}, true);

				window.addEventListener("keyup", function(event) {
					_this.setKeyUp(_this, event);
				}, true);
			},

			setKeyDown : function(_this, event) {
				if(_this.prevent_default) {
					event.preventDefault();
				}
				
				if(_this.stop_propagation) {
					event.stopPropagation();
				}
				
				if(_this.key_logger) {
					$log.updateKey(true, event.keyCode, GSAW.event.GLYPH[event.keyCode]);
				}
				
				_this.keys[event.keyCode] = true;
			},
			
			setKeyUp : function(_this, event) {
				if(_this.prevent_default) {
					event.preventDefault();
				}
				
				if(_this.stop_propagation) {
					event.stopPropagation();
				}
				
				if(_this.key_logger) {
					$log.updateKey(false, event.keyCode, GSAW.event.GLYPH[event.keyCode]);
				}
				_this.keys[event.keyCode] = false;
			}
		};
		keyManager.init(params);

		return keyManager;
	},
	
	/* Class - TouchManager  *
	 * * * * * * * * * * * * *
	 * I'll do some work in this at the weekend as I have better connectivity
	 * then, right now I have no way to test out the functionality of such 
	 * feature, the plan is that the touch manager can handle things like 
	 * mouse position and click events, somewhat simulating a mouse.
	 */
	TouchManager : function() {
		var touchManager = {
				
		};
		return touchManager;
	}
};

GSMouseManager = GSAW.event.MouseManager;
GSKeyManager = GSAW.event.KeyManager;
GSTouchManager = GSAW.event.TouchManager;