GSAW.gl.shader = {
	VERTEX_SHADER : 0,
	FRAGMENT_SHADER : 1,

	ShaderManager : function() {
		var shadermanager = {
			vShader : {},
			fShader : {},
			program : {},

			createXHR : function() {   
				try { return new XMLHttpRequest(); } catch(e) {}
			    try { return new ActiveXObject("Msxml2.XMLHTTP.6.0"); } catch (e) {}
			    try { return new ActiveXObject("Msxml2.XMLHTTP.3.0"); } catch (e) {}
			    try { return new ActiveXObject("Msxml2.XMLHTTP"); } 	  catch (e) {}
			    try { return new ActiveXObject("Microsoft.XMLHTTP"); }  catch (e) {}
									  	           
			    return null;
			},

			fetchShader : function(type, src, callback, _this) {
				var xhr = this.createXHR();
				if(xhr) {
					xhr.open("GET", src, false);
					xhr.onreadystatechange = function() {
						if(xhr.readyState == 4 && xhr.status == 200) {
							callback(xhr, type, _this);
						}
					};
					xhr.send();
				}
			},

			loadShader : function(xhr, type, _this) {
				var shaderScript = xhr.responseText;
				var shader;

				$log.add(_this);
				
				shader = _this.compileShader(shaderScript, type);
				if(type == GSAW.gl.shader.VERTEX_SHADER) {
					_this.vShader = shader;
				} else if(type == GSAW.gl.shader.FRAGMENT_SHADER) {
					_this.fShader = shader;
				}
			},
			
			compileShader : function(shaderSource, type) {
				var shader;
				
				if(type == GSAW.gl.shader.VERTEX_SHADER) {
					shader = gl.createShader(gl.VERTEX_SHADER);
				} else if(type == GSAW.gl.shader.FRAGMENT_SHADER) {
					shader = gl.createShader(gl.FRAGMENT_SHADER);
				} else {
					$log.add("3d", "No valid type specified!");
					return null;
				}
				
				gl.shaderSource(shader, shaderSource);
				gl.compileShader(shader);
				
				if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		           $log.add("3d", gl.getShaderInfoLog(shader));
		            return null;
		        } else {
		        	$log.add("3d", "Shader compiled correctly!");
		        	return shader;
		        }
			},
			
			initShaders : function(vShader, fShader) {
				var _this = this;
				// Fetch the WebGL context, maybe we send it with the method call?

				this.fetchShader(1, fShader, _this.loadShader, _this);
				this.fetchShader(0, vShader, _this.loadShader, _this);

				this.program = gl.createProgram();  
				
				gl.attachShader(this.program, this.vShader);  
				gl.attachShader(this.program, this.fShader);  
				gl.linkProgram(this.program);  

				// If creating the shader program failed, alert  

				if (!gl.getProgramParameter(this.program, gl.LINK_STATUS)) {  
					$log.add("3d", "Unable to initialize the shader program.");  
				} else {
					$log.add("3d", "Shader program initilized without error!");
				}

				gl.useProgram(this.program);
			},	
		};

		return shadermanager;
	}
	
};

GSShaderManager = GSAW.gl.shader.ShaderManager;