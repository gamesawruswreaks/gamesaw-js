/* * * * * * * * * * * * * * * * *
 * GameSaw - A HTML5 Gamelibrary *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * GameSaw is a HTML5 Gamelibrary that makes it easy peasy to prototype and make
 * fullblown games for the next big standard! Hopefully mobile and device support
 * will continue to grow and give us HTML5'ers a viable market to sell and make
 * mooonies on HTML5 games!
 * Here is some pointers on the main class and objects of the GameSaw library:
 * GameSaw comes with a couple of essential parts that will make game makin a whole
 * lot easier, it comes with helpers for both 2d and WebGL rendering as well as
 * common tools and methods for general game logic, such as collition detection,
 * pathfinding, tilemap objects and of course some standard UI components.
 * There's also some very special objects that will help the engine roar and keep
 * track on events and assets for you, these are listed here:
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * $settings : Holds info about general settings for the environment.
 * $resource : Resource manager, keeps track on which resorces has finished loading and such.
 * $key : Keyboard manager, keeps track on which keys are being triggered and released.
 * $mouse : Mouse manager, keeps track on mouse coordinates and mouse buttons.
 * $log : GameSaw's general logger tool, can be setup to log in console or graphical UI!
 * $intersects : Collision help method, takes two shapes and checks if they intersects.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

var GSAW = {
	DEFAULT : "2d",
	WEB_GL : "experimental-webgl",
	LOG_TYPE : {
	},

	Application : function(params) {
		var application = {
			width: 640,
			height: 480,
			renderer: GSAW.DEFAULT,
			targetFps: 60,

			showFps: true,

			container: {},
			resourceManager: {},
			surfaces: {},
			application: {},

			timerId: 0,
			fps: 0,
			lastDelta: 0,
			lastUpdate: 0,
			frames: 0,

			uid: 0,
			activeSurface: 0,
			mouseManager: {},
			keyManager: {},
			touchManager: {},
			globalManagers: true,
			logger: false,

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}

				window.$log = new GSAW.Logger({id: "gs-logger", parent: this.container});

				this.resourceManager = new GSResourceManager();
				this.mouseManager = new GSMouseManager({parent: this, container: this.container});
				this.keyManager = new GSKeyManager({parent: this, container: this.container});

				if(this.globalManagers) {
					window.$resource = this.resourceManager;
					window.$mouse = this.mouseManager;
					window.$key = this.keyManager;
				}

				window.$settings = {
					global: this.globalManagers,
					width: this.width,
					height: this.height,
					container: this.container,
					setContext: this.setContext
				};
				//this.touchManager = new GSTouchManager();
			},

			setApplication : function(app) {
				this.application = app;
			},

			setContext : function(surface) {
				this.activeSurface = surface;
				$g = this.surfaces[surface];
			},

			addSurface : function() {
				if(this.renderer == GSAW.DEFAULT) {
					var surface = new GSSurface2D({parent: this.container, uid: this.uid, width: this.width, height: this.height});
				} else {
					var surface = new GSSurface3D({parent: this.container, uid: this.uid, width: this.width, height: this.height});
					surface.initContext();
				}
				
				this.surfaces[this.uid] = surface;
				this.activeSurface = this.uid;
				this.uid += 1;

				return this.uid;
			},

			removeSurface : function(uid) {
				var surface = this.surfaces[uid];
				surface.deconstructor();
				delete this.surfaces[uid];
			},

			addEventListeners : function() {

			},

			step : function() {
				if(this.renderer == GSAW.DEFAULT) {
					this.update();
				} else {
					this.glUpdate();
				}
			},

			start : function() {
				var _this = this;
				this.timerId = window.setInterval(function(){
					if(_this.renderer == GSAW.DEFAULT) {
						_this.update();
					} else {
						_this.glUpdate();
					}
				}, (1000/_this.targetFps));
			},

			stop : function() {
				window.clearInterval(this.timerId);
			},

			update : function() {
				// Need to calculate the fps here, as well as the frames delta.
				var now = new Date().getTime();
				$g = this.surfaces[this.activeSurface].getContext();
				var delta = now - this.lastDelta;
				this.lastDelta = now;

				this.surfaces[this.activeSurface].clear();
				this.application.update(delta);
				this.application.render(delta);

				// Check if the user wants to render the FPS
				if(this.showFps) {
					this.renderFps();
				}

				if(this.logger) {
					$log.update();
				}

				if(now - this.lastUpdate > 1000) {
					this.fps = this.frames;
					this.frames = 0;
					this.lastUpdate = now;
				} else {
					this.frames += 1;
				}

				this.mouseManager.resetClicks();
			},

			glUpdate : function() {
				var now = new Date().getTime();
				var delta = now - this.lastDelta;
				this.lastDelta = now;

				this.surfaces[this.activeSurface].clear();
				this.application.update(delta);
				this.application.render(delta);

				// Check if the user wants to render the FPS
				//if(this.showFps) {
				//	this.renderFps(context);
				//}

				if(now - this.lastUpdate > 1000) {
					this.fps = this.frames;
					this.frames = 0;
					this.lastUpdate = now;
				} else {
					this.frames += 1;
				}
			},

			renderFps : function() {
				$g.save();
				$g.textAlign = "left";
				$g.font = "10px Helvetica";
				$g.fillStyle = "white";
				$g.fillText("FPS: " + this.fps, 10, 20);
				$g.restore();
			},

			deconstructor : function() {

			}
		};
		application.constructor(params);

		return application;
	},

	Logger : function(params) {
		var logger = {
			id: "gs-logger",
			elements: {
				container: {},
				list: {},
				mouse: {},
				keys: {}
			},
			parent: {},
			msgStyle: {
				borderBottomStyle: "dashed",
				borderBottomWidth: "1px",
				borderBottomColor: "#ddd",
				width: "100%",
				minHeight: "20px",
				clear: "both",
				float: "left",
				padding: "2px 5px"
			},
			liveData: {
				mouse: true,
				keys: true,
			},
			keyStr: "Active Keys: ",
			activeKeys: {
			},

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}

				this.generate();
			},

			generate : function() {
				this.elements.container = document.createElement("div");
				this.elements.container.setAttribute("id", this.id);
				var style = this.elements.container.style;

				style.position = "relative";
				style.borderStyle = "solid";
				style.borderColor = "#000";
				style.borderWidth = "1px";
				style.overflow = "hidden";
				style.minHeight = "200px";

				style.backgroundColor = "#fff";
				style.color = "#000";

				this.parent.parentNode.appendChild(this.elements.container);

				if(this.liveData.mouse) {
					this.elements.mouse = document.createElement("div");
					style = this.elements.mouse.style;
					style.position = "relative";
					style.width = "100%";
					style.clear = "both";
					style.borderBottomStyle = "solid";
					style.borderBottomWidth = "1px";
					style.borderBottomColor = "#ddd";
					style.height = "20px";
					style.padding = "5px";

					this.elements.container.appendChild(this.elements.mouse);
				}

				if(this.liveData.keys) {
					this.elements.keys = document.createElement("div");
					style = this.elements.keys.style;
					style.width = "100%";
					style.clear = "both";
					style.borderBottomStyle = "solid";
					style.borderBottomWidth = "1px";
					style.borderBottomColor = "#ddd";
					style.height = "20px";
					style.padding = "5px";

					this.elements.container.appendChild(this.elements.keys);
				}

				this.elements.list = document.createElement("div");
				style = this.elements.list.style;
				style.position = "absolute";
				style.width = "100%";
				style.top = "62px";
				style.bottom = "0px";
				style.clear = "both";
				style.overflow = "hidden";
				style.overflowY = "scroll";

				this.elements.container.appendChild(this.elements.list);
			},

			update : function(params) {
				if(this.liveData.mouse) {
					var mouseData = "Mouse Position ";
					mouseData += " X: " + $mouse.x;
					mouseData += " Y: " + $mouse.y;

					mouseData += " Buttons [" + $mouse.button[0] + "][" + $mouse.button[1] + "][" + $mouse.button[2] + "]";
					this.elements.mouse.innerHTML = mouseData;
				}

				if(this.liveData.keys) {
					this.elements.keys.innerHTML = this.keyStr;
				}
			},

			updateKey : function(state, code, glyph) {
				if(state) {
					this.activeKeys[code] = glyph;
				} else {
					delete this.activeKeys[code];
				}

				this.keyStr = "Active Keys: ";
				for(keys in this.activeKeys) {
					this.keyStr += this.activeKeys[keys] + "[" + keys + "] ";
				}
			},

			add : function(type, txt, level) {
				var el = document.createElement('div');
				var icon = document.createElement('div');
				var msg = document.createElement('span');

				icon.setAttribute("class", "icon icon-" + type);
				icon.style.float = "left";
				msg.innerHTML = txt;
				msg.style.float = "left";

				el.appendChild(icon);
				el.appendChild(msg);

				for(key in this.msgStyle) {
					el.style[key] = this.msgStyle[key];
				}

				if(level == undefined || level == "info") {
					el.style.backgroundColor = "#b2cbd8";
				} else if(level == "warning") {
					el.style.backgroundColor = "#d5b05f";
				} else {
					el.style.backgroundColor = "#a65252";
				}

				if(!this.elements.list.hasChildNodes()) {
					this.elements.list.appendChild(el);
				} else {
					this.elements.list.insertBefore(el, this.elements.list.childNodes[0]);
				}
			},
		};
		logger.constructor(params);

		return logger;
	}
};

GSApplication = GSAW.Application;