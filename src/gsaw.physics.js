/* *
 * Physics Engine
 * *
 * The GameSaw physics engine keeps track of an array of objects and
 * inflicts upon them physical rules so that they interact in a logical
 * manner, this namespace is heavily dependent on both the geometrical and
 * collision namespaces.
 *
 */

GSAW.engine.physics = {

};