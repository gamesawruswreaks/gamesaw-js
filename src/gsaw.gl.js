/* *
 * WebGL
 * *
 * This part of GameSaw takes care of special WebGL methods and utilities to make
 * WebGL implementation a blast, we have 2 special ways to work with WebGL, either
 * 2d or 3d, let's start of with 3d implementations so we have a good base to work
 * with! I need to do some testing to get a good rendering pipeline going.
 */

GSAW.gl = {

};