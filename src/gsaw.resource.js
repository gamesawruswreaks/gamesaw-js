/* *
 * Resource
 * *
 * A Collection of resource managers for image and audio among other things, these
 * little buggers keeps track of all the resources a application uses so that the
 * developer gets total control of it's lifespan as well as gets notified when all
 * resources are ready to be used!
 */

GSAW.resource = {

	ResourceManager : function(params) {
		var resourcemanager = {
			uid : 0,
			loaded : 0,
			loadedImages : 0,
			loadedAudio : 0,
			numResources : 0,
			numImageResources : 0,
			numAudioResources : 0,
			resources : {},

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			},

			addResource : function(type) {
				var resource = new GSResource({type: type, uid: this.uid});
				this.resources[this.uid] = resource;
				this.uid += 1;
				this.numResources += 1;
				if(type == "image") {
					this.numImageResources += 1;
				} else {
					this.numAudioResources += 1;
				}

				return resource.getId();
			},

			setLoaded : function(uid) {
				this.resources[uid].setLoaded(true);
				this.loaded += 1;
				if(this.resources[uid].getType() == "image") {
					this.loadedImages += 1;
				} else {
					this.loadedAudio += 1;
				}
			},

			countLoaded : function(type) {
				var loaded = 0;

				if(type != undefined) {
					if(type == "image") {
						for(resource in this.resources) {
							var res = this.resources[resource];
							if(res.getType() != "image") {
								continue;
							}

							if(res.getLoaded()) {
								loaded += 1;
							}
						}
					} else {
						for(resource in this.resources) {
							var res = this.resources[resource];
							if(res.getType() != "audio") {
								continue;
							}

							if(res.getLoaded()) {
								loaded += 1;
							}
						}
					}
				} else {
					for(resource in this.resources) {
						var res = this.resources[resource];

						if(res.getLoaded()) {
							loaded += 1;
						}
					}
				}
			},
		};
		resourcemanager.constructor(params);

		return resourcemanager;
	},

	Resource : function(params) {
		var resource = {
			type: "image",
			uid: 0,
			loaded: false,

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			},

			setType : function(type) {
				this.type = type;
			},

			getType : function() {
				return this.type;
			},

			setId : function(id) {
				this.uid = id;
			},

			getId : function() {
				return this.uid;
			},

			setLoaded : function(loaded) {
				this.loaded = loaded;
			},

			getLoaded : function() {
				return this.loaded;
			}
		};
		resource.constructor(params);

		return resource;
	}
};

GSResourceManager = GSAW.resource.ResourceManager;
GSResource = GSAW.resource.Resource;