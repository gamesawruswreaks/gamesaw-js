/* *
 * Audio
 * *
 * The Audio namespace is a collection of audio related classes, at start
 * these are just two, the sample (short clip) and the Track (long clip).
 * these classes will be populated with the usual control methods to keep
 * track and control audio implementations in the application.
 *
 */

GSAW.audio = {

	Track : function(params) {
		var track = {

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		track.constructor(params);

		return track;
	},

	Sample : function(params) {
		var sample = {

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		sample.constructor(params);

		return sample;
	}
};