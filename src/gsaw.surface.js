/* *
 * Surface
 * *
 * There is 2 major surfaces in the gamesaw library, the 2d Surface and
 * the WebGL surface, that pretty much explains what you can do with them
 * both.
 *
 */

GSAW.surface = {

	Surface2D : function(params) {
		var surface2d = {
			uid: 0,
			width: 640,
			height: 480,
			pos: {
				x: 0,
				y: 0,
				z: 1
			},
			parent: {},
			container: {},

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}

				this.generate();
			},

			setId : function(id) {
				this.uid = id;
			},

			generate : function() {
				this.container = document.createElement("canvas");
				this.container.setAttribute("class", "gsaw_surface-" + this.uid);
				this.container.setAttribute("width", this.width);
				this.container.setAttribute("height", this.height);
				this.container.style.position = "absolute";
				this.container.style.top = this.pos.x + "px";
				this.container.style.left = this.pos.y + "px";
				this.container.style.zIndex = this.pos.z;

				this.parent.appendChild(this.container);
			},

			setZIndex : function(index) {
				this.pos.z = index;
				this.container.style.zIndex = index;
			},

			getContext : function() {
				return this.container.getContext("2d");
			},

			clear : function() {
				this.getContext().clearRect(0, 0, this.width, this.height);
			},

			toUrl : function() {
				return this.container.toDataURL("image/png");
			},

			deconstructor : function() {
				var parent = this.container.parentNode;
				parent.removeChild(this.container);
			}
		};
		surface2d.constructor(params);

		return surface2d;
	},

	Surface3D : function(params) {
		var surface3d = {
			uid: 0,
			width: 640,
			height: 480,
			pos: {
				x: 0,
				y: 0
			},
			parent: {},
			container: {},

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}

				this.generate();
			},

			setId : function(id) {
				this.uid = id;
			},

			generate : function() {
				this.container = document.createElement("canvas");
				this.container.setAttribute("class", "gsaw_surface-" + this.uid);
				this.container.setAttribute("width", this.width);
				this.container.setAttribute("height", this.height);
				this.container.style.position = "absolute";
				this.container.style.top = this.pos.x + "px";
				this.container.style.left = this.pos.y + "px";

				this.parent.appendChild(this.container);
			},

			initContext : function() {
				gl = null;
				try {
					gl = this.container.getContext("experimental-webgl", {preserveDrawingBuffer: true}) || this.container.getContext("webgl", {preserveDrawingBuffer: true});
				} catch(e) {
					$log.add("3d", "Doh!, Something went wrong: " + e);
					return 0;
				}

				$log.add("3d", "WebGL initialized successfully!");
				gl.clearColor(0.0, 0.0, 0.0, 1.0);
			},

			clear : function() {
    			gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
				gl.enable(gl.BLEND);
				gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
			},

			toUrl : function() {
				return this.container.toDataURL("image/png");
			},

			deconstructor : function() {
				var parent = this.container.parentNode;
				parent.removeChild(this.container);
			}
		};
		surface3d.constructor(params);

		return surface3d;
	}
};

GSSurface2D = GSAW.surface.Surface2D;
GSSurface3D = GSAW.surface.Surface3D;