/* *
 * Engine
 * *
 * The GameSaw Engine is a placeholder class for the engine parts of
 * the library, this is the core game engine components such as 
 * geometry, collision detection and particle engine / physics engine.
 *
 */
 
GSAW.engine = {

	Utility : {
		EPSILON : 0.000001,
		PI : Math.PI,

		degreeToRadian : function(degree) {
			return degree * (GSPI/180);
		},

		radianToDegree : function(radian) {
			return radian * (180/GSPI);
		},

		capitalize : function(str) {
			return str.charAt(0).toUpperCase() + str.slice(1);
		}
	},

	Math : {
		distance : function() {

		}
	}
};

GSPI = GSAW.engine.Utility.PI;