GSAW.ui = {
	Button : function(params) {
		var button = {
			text : "",
			image : null,
			shape : {},
			hover : false,
			renderMode : "color",
			callback : {},
			payload : 0,
			parent : {},
			color : "#ffffff",
			states : {
				inactive : {
					image : null,
					color : new GSColor({r: 0, g: 0, b: 0, a: 0})
				},
				active : {
					image : null,
					color : new GSColor({r: 0, g: 0, b: 0, a: 0})
				},
				hover : {
					image : null,
					color : new GSColor({r: 0, g: 0, b: 0, a: 0})
				}
			},

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			},

			update : function() {
				var point = new GSPoint({x : $mouse.x, y : $mouse.y});
				this.hover = $intersects(point, this.shape);

				if(this.hover && $mouse.click[0]) {
					if(typeof(this.callback) === "function") {
						this.callback(this.parent, this.payload);
					}
				}
			},

			render : function() {
				// Render ze button!
				$g.save();
				
				if(this.renderMode === "color") {
					$g.fillStyle = this.states.inactive.color.getHex();
					$g.fillRect(this.shape.getX(), this.shape.getY(), this.shape.getWidth(), this.shape.getHeight());
					$g.fillStyle = this.color;
					$g.fillText(this.text, this.shape.getX() + this.shape.getWidth() / 2, this.shape.getY() + this.shape.getHeight() / 2);
				} else if(this.renderMode === "image") {

				} else if(this.renderMode === "combo") {
					$g.fillRect(this.shape.getX(), this.shape.getY(), this.shape.getWidth(), this.shape.getHeight());
					$g.fillText(this.text);
				} else {
					
				}
				$g.restore();
			}
		};
		button.constructor(params);

		return button;
	}
};

GSButton = GSAW.ui.Button;