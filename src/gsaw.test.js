GSAW.test = {
	Suit : function(params) {
		var suit = {
			title : "",
			description : "",
			failed : 0,
			passed : 0,

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}

				$log.add(this.title + " | " + this.description);
			},

			addAssert : function(txt, expect, recived) {
				if(GSAssert(txt, expect, recived)) {
					this.passed += 1;
				} else {
					this.failed += 1;
				}
			},

			report : function() {
				if(this.failed == 0) {
					$log.add("test", "All " + this.passed + " tests passed!", "info");
				} else {
					$log.add("test", this.passed + " tests passed and " + this.failed + " tests failed.", "warning");
				}
			}
		};
		suit.constructor(params);

		return suit;	
	},

	Assert : function(txt, expect, recived) {
		var passed = (expect === recived);
		var level = "fatal";
		if(passed) {
			level = "info";
			txt = txt += ' : <span class="icon icon-pass"> </span>';
		}
		$log.add("test", txt, level);
		return passed;
	},
};

GSAssert = GSAW.test.Assert;
GSSuit = GSAW.test.Suit;