/* *
 * Geometry
 * *
 * The geometry namespace of gamesaw is a collection of geometrical objects
 * that can be used in an array of ways, these are compatible with the gamesaw
 * collision and physics engine as far as it's possible, each class will also
 * have a render method so that the developer can position geometrical shapes
 * in a fast and efficient way.
 *
 */

GSAW.engine.geometry = {

	Vector2D : function(params) {
		var vector2d = {
			x : 0.0,
			y : 0.0,
			type : "vector2d",

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			},

			setX : function(x) {
				this.x = x;
			},

			getX : function() {
				return this.x;
			},

			setY : function(y) {
				this.y = y;
			},

			getY : function() {
				return this.y;
			},

			setPosition : function(x, y) {
				this.x = x;
				this.y = y;
			},

			getPosition : function() {
				return {x: this.x, y: this.y};
			},

			add : function(vec2d) {
				return new GSVector2D(this.x + vec2d.x, this.y + vec2d.y);
			},

			subtract : function(vec2d) {
				return new GSVector2D(this.x - vec2d.x, this.y - vec2d.y);
			},

			invert : function() {
				return new GSVector2D(-this.x, -this.y);
			},

			scale : function(scalar) {
				return new GSVector2D(this.x * scalar, this.y * scalar);
			},

			dot : function(vec2d) {
				return ((this.x * vec2d.x) + (this.y * vec2d.y));
			},

			cross : function(vec2d) {
				return ((this.x * vec2d.y) - (this.y * vec2d.x));
			},

			length : function() {
				return Math.sqrt(this.lengthSquared());
			},

			lengthSquared : function() {
				return (this.x * this.x) + (this.y * this.y);
			},

			normalize : function() {
				var length = this.length();

				if(length == 0) {
					return new GSVector2D(0.0, 0.0);
				}

				return new GSVector2D(this.x / length, this.y / length);
			},

			project : function(vec2d) {
				var dot, length;
				dot = this.dot(vec2d);
				length = this.lengthSquared();

				return new GSVector2D((dot / length) * this.x, (dot / length) * this.y);
			},

			angle : function() {
				return Math.atan2(this.x, -this.y);
			},

			rotate : function(radian) {
				// Must fix a utility class!
				if(this.x == 0) {
					this.x += GSAW.engine.Utility.EPSILON;
				}

				if(this.y == 0) {
					this.y += GSAW.engine.Utility.EPSILON;
				}

				return new GSVector2D(Math.cos(radian) * this.x - Math.sin(radian) * this.y,
					Math.sin(radian) * this.x + Math.cos(radian) * this.y);
			},

			rotatePivot : function(x, y, radian) {
				if(this.x == 0) {
					this.x += GSAW.engine.Utility.EPSILON;
				}
				if(this.y == 0) {
					this.y += GSAW.engine.Utility.EPSILON;
				}

				var x, y;
				srcX = this.x;
				srcY = this.y;

				srcX -= x;
				srcY -= y;

				return new GSVector2D((Math.cos(radian) * srcX - Math.sin(radian) * srcY) + x,
					(Math.sin(radian) * srcX + Math.cos(radian) * srcY) + y);
			},

			render : function() {
				$g.beginPath();
				$g.arc(this.x, this.y, 1.0, 0, Math.PI * 2);
				$g.fill();
			},

			renderDirection : function(x, y) {
				$g.beginPath();
				$g.moveTo(x, y);
				$g.lineTo(x + this.x, y + this.y);
				$g.stroke();
			},

			renderArrow : function(x, y) {
				var temp;

				this.renderDirection(x, y);

				temp = new GSVector2D(this.x, this.y);

				temp = temp.normalize();
				temp = temp.rotate(GSAW.engine.Utility.degreeToRadian(30));

				$g.beginPath();
				$g.moveTo(x + this.x, y + this.y);
				$g.lineTo((x + this.x) - (temp.x * 10), (y + this.y) - (temp.y * 10));
				$g.closePath();
				$g.stroke();

				temp = temp.rotate(GSAW.engine.Utility.degreeToRadian(-60));

				$g.beginPath();
				$g.moveTo(x + this.x, y + this.x);
				$g.lineTo((x + this.x) - (temp.x * 10), (y + this.y) - (temp.y * 10));
				$g.closePath();
				$g.stroke();
			},

			renderDestination : function(x, y) {
				$g.beginPath();
				$g.arc(x + this.x, y + this.y, 2, 0, Math.PI * 2, true);
				$g.fill();
			}
		};
		vector2d.constructor(params);

		return vector2d;
	},

	Vector3D : function(params) {
		var vector3d = {
			x : 0.0,
			y : 0.0,
			z : 0.0,
			type : "vector3d",

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		vector3d.constructor(params);

		return vector3d;
	},

	Point : function(params) {
		var point = {
			x : 0.0,
			y : 0.0,
			type : "point",

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			},

			transform : function(x, y) {
				this.x += x;
				this.y += y;
			},

			setPosition : function(x, y) {
				this.x = x;
				this.y = y;
			},

			getPosition : function() {
				return {x : this.x, y : this.y};
			},

			setX : function(x) {
				this.x = x;
			},

			getX : function() {
				return this.x;
			},

			setY : function(y) {
				this.y = y;
			},

			getY : function() {
				return this.y;
			},

			render : function() {
				$g.beginPath();
				$g.arc(this.x, this.y, 1.0, 0, Math.PI * 2);
				$g.fill();
			}
		};
		point.constructor(params);

		return point;
	},

	Line : function(params) {
		var line = {
			p0 : new GSPoint({x: 0.0, y: 0.0}),
			p1 : new GSPoint({x: 1.0, y: 1.0}),
			type : "line",

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			},

			transform : function(x, y) {
				this.p0.transform(x, y);
				this.p1.transform(x, y);
			},

			transformStart : function(x, y) {
				this.p0.transform(x, y);
			},

			transformEnd : function(x, y) {
				this.p1.transform(x, y);
			},

			setPosition : function(x0, y0, x1, y1) {
				this.p0.setPosition(x0, y0);
				this.p1.setPosition(x1, y1);
			},

			render : function() {
				$g.beginPath();
				$g.moveTo(this.p0.x, this.p0.y);
				$g.lineTo(this.p1.x, this.p1.y);
				$g.stroke();
			}
		};
		line.constructor(params);

		return line;
	},

	Triangle : function(params) {
		var triangle = {
			p0 : new GSPoint({x: 1.0, y: 0.0}),
			p1 : new GSPoint({x: 1.0, y: 1.0}),
			p2 : new GSPoint({x: 0.0, y: 1.0}),
			type : "triangle",

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			},

			transform : function(x, y) {
				this.p0.transform(x, y);
				this.p1.transform(x, y);
				this.p2.transform(x, y);
			},

			setPosition : function(point, x, y) {
				this['p' + point].setPosition(x, y);
			},

			render : function() {
				$g.beginPath();
				$g.moveTo(this.p0.x, this.p0.y);
				$g.lineTo(this.p1.x, this.p1.y);
				$g.lineTo(this.p2.x, this.p2.y);
				$g.lineTo(this.p0.x, this.p0.y);
				$g.stroke();
			}
		};
		triangle.constructor(params);

		return triangle;
	},

	Rectangle : function(params) {
		var rectangle = {
			pos : new GSPoint({x: 0.0, y: 0.0}),
			width : 1.0,
			height : 1.0,
			type : "rectangle",

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			},

			transform : function(x, y) {
				this.pos.transform(x, y);
			},

			setPosition : function(x, y) {
				this.pos.setPosition(x, y);
			},

			setWidth : function(width) {
				this.width = width;
			},

			getX : function() {
				return this.pos.getX();
			},

			getY : function() {
				return this.pos.getY();
			},

			getWidth : function() {
				return this.width;
			},

			setHeight : function(height) {
				this.height = height;
			},

			getHeight : function() {
				return this.height;
			},

			render : function() {
				$g.strokeRect(this.pos.x, this.pos.y, this.width, this.height)
			}
		};
		rectangle.constructor(params);

		return rectangle;
	},

	AABB : function(params) {
		var aabb = {
			pos : new GSPoint({x: 0.0, y: 0.0}),
			hWidth : 1.0,
			hHeight : 1.0,
			type : "aabb",

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			},

			transform : function(x, y) {
				this.pos.transform(x, y);
			},

			setPosition : function(x, y) {
				this.pos.setPosition(x, y);
			},

			setHalfWidth : function(hWidth) {
				this.hWidth = hWidth;
			},

			getHalfWidth : function() {
				return this.hWidth;
			},

			setHalfHeight : function(hHeight) {
				this.hHeight = hHeight;
			},

			getHalfHeight : function() {
				return this.hHeight;
			},

			render : function() {
				$g.strokeRect(this.pos.x - this.hWidth, this.pos.y - this.hHeight, this.hWidth * 2, this.hHeight * 2);
			}
		};
		aabb.constructor(params);

		return aabb;
	},

	Plane : function(params) {
		var plane = {
			type : "plane",

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		plane.constructor(params);

		return plane;
	},

	Circle : function(params) {
		var circle = {
			pos : new GSPoint({x: 0.0, y: 0.0}),
			radius : 1.0,
			type : "circle",

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			},

			transform : function(x, y) {
				this.pos.transform(x, y);
			},

			setPosition : function(x, y) {
				this.pos.setPosition(x, y);
			},

			setX : function(x) {
				this.pos.setX(x);
			},

			getX : function() {
				return this.pos.getX();
			},

			setY : function(y) {
				this.pos.setY(y);
			},

			getY : function() {
				return this.pos.getY();
			},

			setRadius : function(radius) {
				this.radius = radius;
			},

			getRadius : function() {
				return this.radius;
			},

			render : function() {
				$g.beginPath();
				$g.arc(this.pos.x, this.pos.y, this.radius, 0, Math.PI * 2);
				$g.stroke();
			}
		};
		circle.constructor(params);

		return circle;
	},

	Polygon : function(params) {
		var polygon = {
			points : [],
			type : "polygon",

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			},

			addPoint : function(point) {
				this.points.push(point);
			},

			removePoint : function(i) {
				this.points.slice(i, 1);
			},

			removeFirst : function() {
				this.points.shift();
			},

			removeLast : function() {
				this.points.pop();
			},

			render : function() {
				var len, i;
				i = 1;
				len = this.points.length;

				if(len < 3) {
					return 0;
				}

				$g.beginPath();
				$g.moveTo(this.points[0].x, this.points[0].y);
				for(i; i < len; i += 1) {
					$g.lineTo(this.points[i].x, this.points[i].y);
				}
				$g.lineTo(this.points[0].x, this.points[0].y)
				$g.stroke();
			}
		};
		polygon.constructor(params);

		return polygon;
	},

	Bezier : function(params) {
		var bezier = {
			p0 : new GSVector2D({x: 0.0, y: 0.0}),
			p1 : new GSVector2D({x: 0.0, y: 1.0}),
			c0 : new GSVector2D({x: 1.0, y: 0.0}),
			c1 : new GSVector2D({x: 1.0, y: 1.0}),
			type : "bezier",

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		bezier.constructor(params);

		return bezier;
	},

	Path : function(params) {
		var path = {
			segments : {},
			type : "path",

			constructor : function(params) {
				for(key in params) {
					if(this[key] != undefined) {
						this[key] = params[key];
					}
				}
			}
		};
		path.constructor(params);

		return path;
	}
};

GSVector2D = GSAW.engine.geometry.Vector2D;
GSVector3D = GSAW.engine.geometry.Vector3D;
GSPoint = GSAW.engine.geometry.Point;
GSLine = GSAW.engine.geometry.Line;
GSTriangle = GSAW.engine.geometry.Triangle;
GSRectangle = GSAW.engine.geometry.Rectangle;
GSCircle = GSAW.engine.geometry.Circle;
GSPolygon = GSAW.engine.geometry.Polygon;
GSAABB = GSAW.engine.geometry.AABB;
GSBezier = GSAW.engine.geometry.Bezier;
GSPath = GSAW.engine.geometry.Path;